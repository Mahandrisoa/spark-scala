package com.cesi.wordCount

import org.apache.spark.sql.SparkSession

object WordCount {
    def main(args: Array[String]) = {
        val urlParam = args(0)
        val times = args(1).toInt
        val spark = SparkSession.builder()
                        .appName("WordCount")
                        .getOrCreate();
        val sc = spark.sparkContext
                        
        import spark.implicits._
        
        val results = getUrlResults(urlParam, times)
        val rdd = sc.parallelize(results)
        val wordCounts = rdd.groupBy(identity).mapValues(_.size)
        
        println("=========== printing results ================ ")
        wordCounts.collect().map(line => println(line._1+ " : " + line._2))

	spark.stop()
    }
    
    def getUrlResults(url: String, times: Int): Array[String] = {
    	var results: Array[String] = Array[String]()
    	for(i <- 1 to times) results = results ++ getContentStrings(url)
    	results
    }
    
    def getContentStrings(url: String): Array[String] = scala.io.Source.fromURL(url).mkString.split(" ")
    
}
